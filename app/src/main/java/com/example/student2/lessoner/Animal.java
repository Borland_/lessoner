package com.example.student2.lessoner;

/**
 * Created by student2 on 10.11.16.
 */

public abstract class Animal {
    public abstract void eat();
    public abstract void sleep ();

    public abstract String getName ();
    public abstract int getImageId ();

    protected void someFunction (){

    }
}
