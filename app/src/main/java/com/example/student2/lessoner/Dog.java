package com.example.student2.lessoner;

/**
 * Created by student2 on 10.11.16.
 */

public class Dog extends Animal {

    private static final String LOG_TAG = "Dog : ";

    private String name;
    private int imageid = R.drawable.dog;

    public Dog (String name)  {
        this.name = name;

    }

    public void eat (){
        Log.d(LOG_TAG, "dog eat");
    }

    public void sleep (){

    }



    @Override
    public String toString (){
        return "This is dog object";
    }

    public String getName(){
        return name;
    }

    public int getImageid(){
        return imageid;
    }
}
